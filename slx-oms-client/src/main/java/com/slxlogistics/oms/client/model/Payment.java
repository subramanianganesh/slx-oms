package com.slxlogistics.oms.client.model;

import java.io.Serializable;
import java.sql.Timestamp;
public class Payment implements Serializable {
    private Long userId = null;
    private Type type = null;
    public enum Type { payment, refund, }; 
    private Status status = null;
    public enum Status { initiated, cancelled, received, failed, successful, }; 
    private Method method = null;
    public enum Method { credit_card, debit_card, netbanking, wallet, }; 
    private String gateway = null;
    private String transactionId = null;
    private Double amount = null;
    private Double actualAmount = null;
    private Timestamp recordedDate = null;
    private Boolean deleted = null;
    private Long id = null;
    public Payment() {
    }

    public Payment(Builder builder) {
        this.userId = builder.userId;
        this.type = builder.type;
        this.status = builder.status;
        this.method = builder.method;
        this.gateway = builder.gateway;
        this.transactionId = builder.transactionId;
        this.amount = builder.amount;
        this.actualAmount = builder.actualAmount;
        this.recordedDate = builder.recordedDate;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public Method getMethod() {
        return method;
    }
    public void setMethod(Method method) {
        this.method = method;
    }

    public String getGateway() {
        return gateway;
    }
    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getActualAmount() {
        return actualAmount;
    }
    public void setActualAmount(Double actualAmount) {
        this.actualAmount = actualAmount;
    }

    public Timestamp getRecordedDate() {
        return recordedDate;
    }
    public void setRecordedDate(Timestamp recordedDate) {
        this.recordedDate = recordedDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Payment {\n");
        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  type: ").append(type).append("\n");
        sb.append("  status: ").append(status).append("\n");
        sb.append("  method: ").append(method).append("\n");
        sb.append("  gateway: ").append(gateway).append("\n");
        sb.append("  transactionId: ").append(transactionId).append("\n");
        sb.append("  amount: ").append(amount).append("\n");
        sb.append("  actualAmount: ").append(actualAmount).append("\n");
        sb.append("  recordedDate: ").append(recordedDate).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Long userId = null;

        private Type type = null;

        private Status status = null;

        private Method method = null;

        private String gateway = null;

        private String transactionId = null;

        private Double amount = null;

        private Double actualAmount = null;

        private Timestamp recordedDate = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setUserId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder setType(Type type) {
            this.type = type;
            return this;
        }

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        public Builder setMethod(Method method) {
            this.method = method;
            return this;
        }

        public Builder setGateway(String gateway) {
            this.gateway = gateway;
            return this;
        }

        public Builder setTransactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder setAmount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder setActualAmount(Double actualAmount) {
            this.actualAmount = actualAmount;
            return this;
        }

        public Builder setRecordedDate(Timestamp recordedDate) {
            this.recordedDate = recordedDate;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Payment build() {
            return new Payment(this); 
        }
    }
}

