package com.slxlogistics.oms.client.model;

import java.io.Serializable;
public class OrderNote implements Serializable {
    private String note = null;
    private String createdBy = null;
    private Boolean deleted = null;
    private Long id = null;
    public OrderNote() {
    }

    public OrderNote(Builder builder) {
        this.note = builder.note;
        this.createdBy = builder.createdBy;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderNote {\n");
        sb.append("  note: ").append(note).append("\n");
        sb.append("  createdBy: ").append(createdBy).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private String note = null;

        private String createdBy = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setNote(String note) {
            this.note = note;
            return this;
        }

        public Builder setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public OrderNote build() {
            return new OrderNote(this); 
        }
    }
}

