package com.slxlogistics.oms.client.model;

import java.io.Serializable;
public class OrderAdjustment implements Serializable {
    private Type type = null;
    public enum Type { CASHBACK, GIFTWRAP, FEE, }; 
    private Double value = null;
    private Boolean deleted = null;
    private Long id = null;
    public OrderAdjustment() {
    }

    public OrderAdjustment(Builder builder) {
        this.type = builder.type;
        this.value = builder.value;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }
    public void setValue(Double value) {
        this.value = value;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderAdjustment {\n");
        sb.append("  type: ").append(type).append("\n");
        sb.append("  value: ").append(value).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Type type = null;

        private Double value = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setType(Type type) {
            this.type = type;
            return this;
        }

        public Builder setValue(Double value) {
            this.value = value;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public OrderAdjustment build() {
            return new OrderAdjustment(this); 
        }
    }
}

