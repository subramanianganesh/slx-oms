package com.slxlogistics.oms.client.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;
public class OrderItem implements Serializable {
    private Status status = null;
    public enum Status { on_hold, created, approved, confirmed, picked, delivered, returned, cancelled, }; 
    private Map<String, String> attributes = null;
    private Set<OrderItemAdjustment> adjustments = null;
    private Set<OrderItemNote> notes = null;
    private Set<OrderItemRequest> requests = null;
    private Long serviceId = null;
    private Long serviceabilityId = null;
    private Double weight = null;
    private String description = null;
    private Double total = null;
    private Timestamp orderDate = null;
    private Slot pickupSlot = null;
    private Slot deliverSlot = null;
    private Double price = null;
    private String trackingId = null;
    private Boolean deleted = null;
    private Long id = null;
    public OrderItem() {
    }

    public OrderItem(Builder builder) {
        this.status = builder.status;
        this.attributes = builder.attributes;
        this.adjustments = builder.adjustments;
        this.notes = builder.notes;
        this.requests = builder.requests;
        this.serviceId = builder.serviceId;
        this.serviceabilityId = builder.serviceabilityId;
        this.weight = builder.weight;
        this.description = builder.description;
        this.total = builder.total;
        this.orderDate = builder.orderDate;
        this.pickupSlot = builder.pickupSlot;
        this.deliverSlot = builder.deliverSlot;
        this.price = builder.price;
        this.trackingId = builder.trackingId;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Set<OrderItemAdjustment> getAdjustments() {
        return adjustments;
    }
    public void setAdjustments(Set<OrderItemAdjustment> adjustments) {
        this.adjustments = adjustments;
    }

    public Set<OrderItemNote> getNotes() {
        return notes;
    }
    public void setNotes(Set<OrderItemNote> notes) {
        this.notes = notes;
    }

    public Set<OrderItemRequest> getRequests() {
        return requests;
    }
    public void setRequests(Set<OrderItemRequest> requests) {
        this.requests = requests;
    }

    public Long getServiceId() {
        return serviceId;
    }
    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Long getServiceabilityId() {
        return serviceabilityId;
    }
    public void setServiceabilityId(Long serviceabilityId) {
        this.serviceabilityId = serviceabilityId;
    }

    public Double getWeight() {
        return weight;
    }
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Double getTotal() {
        return total;
    }
    public void setTotal(Double total) {
        this.total = total;
    }

    public Timestamp getOrderDate() {
        return orderDate;
    }
    public void setOrderDate(Timestamp orderDate) {
        this.orderDate = orderDate;
    }

    public Slot getPickupSlot() {
        return pickupSlot;
    }
    public void setPickupSlot(Slot pickupSlot) {
        this.pickupSlot = pickupSlot;
    }

    public Slot getDeliverSlot() {
        return deliverSlot;
    }
    public void setDeliverSlot(Slot deliverSlot) {
        this.deliverSlot = deliverSlot;
    }

    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTrackingId() {
        return trackingId;
    }
    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderItem {\n");
        sb.append("  status: ").append(status).append("\n");
        sb.append("  attributes: ").append(attributes).append("\n");
        sb.append("  adjustments: ").append(adjustments).append("\n");
        sb.append("  notes: ").append(notes).append("\n");
        sb.append("  requests: ").append(requests).append("\n");
        sb.append("  serviceId: ").append(serviceId).append("\n");
        sb.append("  serviceabilityId: ").append(serviceabilityId).append("\n");
        sb.append("  weight: ").append(weight).append("\n");
        sb.append("  description: ").append(description).append("\n");
        sb.append("  total: ").append(total).append("\n");
        sb.append("  orderDate: ").append(orderDate).append("\n");
        sb.append("  pickupSlot: ").append(pickupSlot).append("\n");
        sb.append("  deliverSlot: ").append(deliverSlot).append("\n");
        sb.append("  price: ").append(price).append("\n");
        sb.append("  trackingId: ").append(trackingId).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Status status = null;

        private Map<String, String> attributes = null;

        private Set<OrderItemAdjustment> adjustments = null;

        private Set<OrderItemNote> notes = null;

        private Set<OrderItemRequest> requests = null;

        private Long serviceId = null;

        private Long serviceabilityId = null;

        private Double weight = null;

        private String description = null;

        private Double total = null;

        private Timestamp orderDate = null;

        private Slot pickupSlot = null;

        private Slot deliverSlot = null;

        private Double price = null;

        private String trackingId = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        public Builder setAttributes(Map<String, String> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder setAdjustments(Set<OrderItemAdjustment> adjustments) {
            this.adjustments = adjustments;
            return this;
        }

        public Builder setNotes(Set<OrderItemNote> notes) {
            this.notes = notes;
            return this;
        }

        public Builder setRequests(Set<OrderItemRequest> requests) {
            this.requests = requests;
            return this;
        }

        public Builder setServiceId(Long serviceId) {
            this.serviceId = serviceId;
            return this;
        }

        public Builder setServiceabilityId(Long serviceabilityId) {
            this.serviceabilityId = serviceabilityId;
            return this;
        }

        public Builder setWeight(Double weight) {
            this.weight = weight;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setTotal(Double total) {
            this.total = total;
            return this;
        }

        public Builder setOrderDate(Timestamp orderDate) {
            this.orderDate = orderDate;
            return this;
        }

        public Builder setPickupSlot(Slot pickupSlot) {
            this.pickupSlot = pickupSlot;
            return this;
        }

        public Builder setDeliverSlot(Slot deliverSlot) {
            this.deliverSlot = deliverSlot;
            return this;
        }

        public Builder setPrice(Double price) {
            this.price = price;
            return this;
        }

        public Builder setTrackingId(String trackingId) {
            this.trackingId = trackingId;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public OrderItem build() {
            return new OrderItem(this); 
        }
    }
}

