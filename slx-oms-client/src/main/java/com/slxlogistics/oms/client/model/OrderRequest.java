package com.slxlogistics.oms.client.model;

import java.io.Serializable;
public class OrderRequest implements Serializable {
    private String reason = null;
    private RequestType type = null;
    public enum RequestType { cancel, hold, }; 
    private String createdBy = null;
    private Boolean deleted = null;
    private Long id = null;
    public OrderRequest() {
    }

    public OrderRequest(Builder builder) {
        this.reason = builder.reason;
        this.type = builder.type;
        this.createdBy = builder.createdBy;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }

    public RequestType getType() {
        return type;
    }
    public void setType(RequestType type) {
        this.type = type;
    }

    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderRequest {\n");
        sb.append("  reason: ").append(reason).append("\n");
        sb.append("  type: ").append(type).append("\n");
        sb.append("  createdBy: ").append(createdBy).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private String reason = null;

        private RequestType type = null;

        private String createdBy = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setReason(String reason) {
            this.reason = reason;
            return this;
        }

        public Builder setType(RequestType type) {
            this.type = type;
            return this;
        }

        public Builder setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public OrderRequest build() {
            return new OrderRequest(this); 
        }
    }
}

