package com.slxlogistics.oms.client.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;
public class Order implements Serializable {
    private Long userId = null;
    private Long sourceAddressId = null;
    private Long destinationAddressId = null;
    private Set<OrderItem> items = null;
    private Set<OrderAdjustment> adjustments = null;
    private Set<OrderNote> notes = null;
    private Set<Payment> payments = null;
    private Set<OrderRequest> requests = null;
    private Double total = null;
    private Status status = null;
    public enum Status { created, approved, confirmed, picked, delivered, cancelled, returned, completed, on_hold, }; 
    private String externalId = null;
    private Map<String, String> attributes = null;
    private Timestamp orderDate = null;
    private Boolean deleted = null;
    private Long id = null;
    public Order() {
    }

    public Order(Builder builder) {
        this.userId = builder.userId;
        this.sourceAddressId = builder.sourceAddressId;
        this.destinationAddressId = builder.destinationAddressId;
        this.items = builder.items;
        this.adjustments = builder.adjustments;
        this.notes = builder.notes;
        this.payments = builder.payments;
        this.requests = builder.requests;
        this.total = builder.total;
        this.status = builder.status;
        this.externalId = builder.externalId;
        this.attributes = builder.attributes;
        this.orderDate = builder.orderDate;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSourceAddressId() {
        return sourceAddressId;
    }
    public void setSourceAddressId(Long sourceAddressId) {
        this.sourceAddressId = sourceAddressId;
    }

    public Long getDestinationAddressId() {
        return destinationAddressId;
    }
    public void setDestinationAddressId(Long destinationAddressId) {
        this.destinationAddressId = destinationAddressId;
    }

    public Set<OrderItem> getItems() {
        return items;
    }
    public void setItems(Set<OrderItem> items) {
        this.items = items;
    }

    public Set<OrderAdjustment> getAdjustments() {
        return adjustments;
    }
    public void setAdjustments(Set<OrderAdjustment> adjustments) {
        this.adjustments = adjustments;
    }

    public Set<OrderNote> getNotes() {
        return notes;
    }
    public void setNotes(Set<OrderNote> notes) {
        this.notes = notes;
    }

    public Set<Payment> getPayments() {
        return payments;
    }
    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }

    public Set<OrderRequest> getRequests() {
        return requests;
    }
    public void setRequests(Set<OrderRequest> requests) {
        this.requests = requests;
    }

    public Double getTotal() {
        return total;
    }
    public void setTotal(Double total) {
        this.total = total;
    }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public String getExternalId() {
        return externalId;
    }
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Timestamp getOrderDate() {
        return orderDate;
    }
    public void setOrderDate(Timestamp orderDate) {
        this.orderDate = orderDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Order {\n");
        sb.append("  userId: ").append(userId).append("\n");
        sb.append("  sourceAddressId: ").append(sourceAddressId).append("\n");
        sb.append("  destinationAddressId: ").append(destinationAddressId).append("\n");
        sb.append("  items: ").append(items).append("\n");
        sb.append("  adjustments: ").append(adjustments).append("\n");
        sb.append("  notes: ").append(notes).append("\n");
        sb.append("  payments: ").append(payments).append("\n");
        sb.append("  requests: ").append(requests).append("\n");
        sb.append("  total: ").append(total).append("\n");
        sb.append("  status: ").append(status).append("\n");
        sb.append("  externalId: ").append(externalId).append("\n");
        sb.append("  attributes: ").append(attributes).append("\n");
        sb.append("  orderDate: ").append(orderDate).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Long userId = null;

        private Long sourceAddressId = null;

        private Long destinationAddressId = null;

        private Set<OrderItem> items = null;

        private Set<OrderAdjustment> adjustments = null;

        private Set<OrderNote> notes = null;

        private Set<Payment> payments = null;

        private Set<OrderRequest> requests = null;

        private Double total = null;

        private Status status = null;

        private String externalId = null;

        private Map<String, String> attributes = null;

        private Timestamp orderDate = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setUserId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder setSourceAddressId(Long sourceAddressId) {
            this.sourceAddressId = sourceAddressId;
            return this;
        }

        public Builder setDestinationAddressId(Long destinationAddressId) {
            this.destinationAddressId = destinationAddressId;
            return this;
        }

        public Builder setItems(Set<OrderItem> items) {
            this.items = items;
            return this;
        }

        public Builder setAdjustments(Set<OrderAdjustment> adjustments) {
            this.adjustments = adjustments;
            return this;
        }

        public Builder setNotes(Set<OrderNote> notes) {
            this.notes = notes;
            return this;
        }

        public Builder setPayments(Set<Payment> payments) {
            this.payments = payments;
            return this;
        }

        public Builder setRequests(Set<OrderRequest> requests) {
            this.requests = requests;
            return this;
        }

        public Builder setTotal(Double total) {
            this.total = total;
            return this;
        }

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        public Builder setExternalId(String externalId) {
            this.externalId = externalId;
            return this;
        }

        public Builder setAttributes(Map<String, String> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder setOrderDate(Timestamp orderDate) {
            this.orderDate = orderDate;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Order build() {
            return new Order(this); 
        }
    }
}

