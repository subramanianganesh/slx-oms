package com.slxlogistics.oms.client.model;

import java.io.Serializable;
import java.sql.Timestamp;
public class Slot implements Serializable {
    private Timestamp beginTime = null;
    private Timestamp endTime = null;
    public Slot() {
    }

    public Slot(Builder builder) {
        this.beginTime = builder.beginTime;
        this.endTime = builder.endTime;
        }

    public Timestamp getBeginTime() {
        return beginTime;
    }
    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }
    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Slot {\n");
        sb.append("  beginTime: ").append(beginTime).append("\n");
        sb.append("  endTime: ").append(endTime).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Timestamp beginTime = null;

        private Timestamp endTime = null;

        public Builder setBeginTime(Timestamp beginTime) {
            this.beginTime = beginTime;
            return this;
        }

        public Builder setEndTime(Timestamp endTime) {
            this.endTime = endTime;
            return this;
        }

        public Slot build() {
            return new Slot(this); 
        }
    }
}

