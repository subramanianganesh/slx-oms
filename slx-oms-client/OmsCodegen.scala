/**
 *  Copyright 2014 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import com.wordnik.swagger.codegen.BasicJavaGenerator
import com.wordnik.swagger.codegen.model._
import scala.collection.immutable.Map
import scala.collection.mutable.{ ListBuffer, HashMap, HashSet }

object OmsCodegen extends BasicJavaGenerator {
  def main(args: Array[String]) = generateClient(args)

  // location of templates
  override def templateDir = System.getenv("TEMPLATE_DIR")

  def clientDir = System.getenv("CLIENT_DIR")
  // where to write generated code
  override def destinationDir = clientDir + "/src/main/java"

  // package for models
  override def modelPackage = Some("com.slxlogistics.oms.client.model")

  // package for api classes
  override def apiPackage = Some("com.slxlogistics.oms.client.api")

  additionalParams ++= Map(
    "artifactId" -> "slx-oms-client",
    "artifactVersion" -> "0.0.1-SNAPSHOT",
    "groupId" -> "com.slxlogistics",
    "parentArtificatId" -> "slx-oms")

  // supporting classes
  override def supportingFiles =
    List(("pom.mustache", clientDir, "pom.xml"))

  override def defaultIncludes = super.defaultIncludes ++ Set("Map", "BigDecimal", "Coordinate", "Location")

  override def importMapping = super.importMapping ++ Map("Map" -> "java.util.Map", "BigDecimal" -> "java.math.BigDecimal",
    "Location" -> "com.slxlogistics.commons.client.model.Location", "Coordinate" -> "com.slxlogistics.commons.client.model.Coordinate")

  override def processImports(ii: Set[Map[String, AnyRef]]) = {
    val imports = new ListBuffer[Map[String, String]]
    super.processImports(ii).foreach(entry => {
      if (entry("import").indexOf("[") < 0) {
        imports += entry
      }
      if (entry("import").indexOf("Map[") >= 0) {
        imports += Map("import" -> "java.util.Map")
      }
    })
    imports
  }

  override def toDeclaration(obj: ModelProperty) = {
    var declaredType = toDeclaredType(obj.`type`)
    if (declaredType.indexOf("Map<") >= 0) {
      val keyType = declaredType.substring(declaredType.indexOf("<") + 1, declaredType.indexOf(","))
      val valueType = declaredType.substring(declaredType.indexOf(",") + 1, declaredType.indexOf(">"))
      declaredType = "Map<" + toDeclaredType(keyType) + ", " + toDeclaredType(valueType) + ">"
      val defaultValue = super.toDefaultValue(declaredType, obj)
      (declaredType, defaultValue)
    } else {
      super.toDeclaration(obj)
    }
  }

  def toCamelCase(name: String) = ("[_|\\.]([a-z\\d])").r.replaceAllIn(name, { m =>
    m.group(1).toUpperCase()
  })

  override def toGetter(name: String, dataType: String) = {
    super.toGetter(toCamelCase(name), dataType)
  }

  override def toSetter(name: String, dataType: String) = {
    super.toSetter(toCamelCase(name), dataType)
  }

  override def toVarName(name: String) = {
    super.toVarName(toCamelCase(name))
  }

  override def toApiName(name: String) = {
    super.toApiName(toCamelCase(name))
  }
  
  override def processApiMap(m: Map[String, AnyRef]): Map[String, AnyRef] = {
    val map = collection.mutable.Map[String, AnyRef]() ++= m
    map ++= Map("paramClassName" -> (map.getOrElse("nickname", map("httpMethod")).asInstanceOf[String].capitalize + "Params"))
    map.toMap
  }
}
