/**
 * 
 */
package com.slxlogistics.oms.domain;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.sql.Timestamp;

import org.activejpa.entity.testng.BaseModelTest;
import org.activejpa.jpa.JPA;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.Sets;
import com.slxlogistics.oms.OMSException;
import com.slxlogistics.oms.domain.Order.Status;

/**
 * @author ganeshs
 * 
 */
public class OrderTest extends BaseModelTest {
	
	private Order order;
	
	private OrderItem orderItem;

	@BeforeClass
	public void beforeClass() {
		JPA.instance.addPersistenceUnit("test");
	}
	
	public void setup() throws Exception {
		super.setup();
		order = new Order(1L, RandomStringUtils.random(5), 2L, 3L, new Timestamp(System.currentTimeMillis()));
		orderItem = new OrderItem(2L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem.setOrder(order);
		order.setItems(Sets.newHashSet(orderItem));
		order.persist();
	}
	
	@Test
	public void shouldCheckIfOrderCanBeApproved() {
		for (Status status : Status.values()) {
			order.setStatus(status);
			if (Order.APPROVABLE_STATES.contains(status)) {
				assertTrue(order.canApprove());
			} else {
				assertFalse(order.canApprove());
			}
		}
	}
	
	@Test
	public void shouldCheckIfOrderCanBeConfirmed() {
		for (Status status : Status.values()) {
			order.setStatus(status);
			if (status == Status.approved) {
				assertTrue(order.canConfirm());
			} else {
				assertFalse(order.canConfirm());
			}
		}
	}
	
	@Test
	public void shouldCheckIfOrderCanBeCancelled() {
		for (Status status : Status.values()) {
			order.setStatus(status);
			if (Order.CANCELLABLE_STATES.contains(status)) {
				assertTrue(order.canCancel());
			} else {
				assertFalse(order.canCancel());
			}
		}
	}
	
	@Test
	public void shouldCheckIfOrderTotalCostCanBeComputed() {
		for (Status status : Status.values()) {
			order.setStatus(status);
			if (Order.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				assertTrue(order.canComputeTotalCost());
			} else {
				assertFalse(order.canComputeTotalCost());
			}
		}
	}
	
	@Test
	public void shouldAddOrderItemIfOrderIsInCreatedState() {
		OrderItem item = new OrderItem(3L, 4L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		order.addItem(item);
		order.persist();
		assertEquals(order.getItems().size(), 2);
		assertTrue(order.getItems().contains(item));
	}
	
	@Test
	public void shouldNotAddOrderItemIfOrderIsNotInCreatedState() {
		OrderItem item = new OrderItem(3L, 4L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		for (Status status : Status.values()) {
			if (status != Status.created) {
				order.setStatus(status);
				try {
					order.addItem(item);
					fail("Expected an OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldRemoveOrderItemIfOrderIsInCreatedState() {
		order.removeItem(orderItem);
		order.persist();
		assertEquals(order.getItems().size(), 0);
		assertFalse(order.getItems().contains(orderItem));
	}
	
	@Test
	public void shouldNotRemoveOrderItemIfOrderIsNotInCreatedState() {
		for (Status status : Status.values()) {
			if (status != Status.created) {
				order.setStatus(status);
				try {
					order.removeItem(orderItem);
					fail("Expected an OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldComputeTotalCostAfterAddingAnItem() {
		Double total = order.computeTotalCost();
		OrderItem item = new OrderItem(3L, 4L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		order.addItem(item);
		assertEquals(total + item.getPrice() , order.getTotal());
	}
	
	@Test
	public void shouldComputeTotalCostAfterRemovingAnItem() {
		Double total = order.computeTotalCost();
		order.removeItem(orderItem);
		assertEquals(total - orderItem.getPrice(), order.getTotal());
	}
	
	@Test
	public void shouldAddAdjustmentsIfTotalCanBeComputed() {
		for (Status status : Status.values()) {
			if (Order.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				order.setStatus(status);
				OrderAdjustment adjustment = new OrderAdjustment(OrderAdjustment.Type.FEE, 10d);
				order.addAdjustment(adjustment);
				order.persist();
				assertTrue(order.getAdjustments().contains(adjustment));
			}
		}
	}
	
	@Test
	public void shouldNotAddAdjustmentsIfTotalCantBeComputed() {
		for (Status status : Status.values()) {
			if (! Order.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				order.setStatus(status);
				OrderAdjustment adjustment = new OrderAdjustment(OrderAdjustment.Type.FEE, 10d);
				try {
					order.addAdjustment(adjustment);
					fail("Expected OMSException for the stauts " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldRemoveAdjustmentIfTotalCanBeComputed() {
		for (Status status : Status.values()) {
			OrderAdjustment adjustment = new OrderAdjustment(OrderAdjustment.Type.FEE, 10d);
			order.addAdjustment(adjustment);
			order.persist();
			if (Order.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				order.setStatus(status);
				order.removeAdjustment(adjustment);
				order.persist();
				assertTrue(! order.getAdjustments().contains(adjustment));
			}
		}
	}
	
	@Test
	public void shouldNotRemoveAdjustmentIfTotalCantBeComputed() {
		OrderAdjustment adjustment = new OrderAdjustment(OrderAdjustment.Type.FEE, 10d);
		order.addAdjustment(adjustment);
		order.persist();
		for (Status status : Status.values()) {
			if (! Order.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				order.setStatus(status);
				try {
					order.removeAdjustment(adjustment);
					fail("Expected OMSException for the stauts " + status);
				} catch (OMSException e) {
					
				}
			}
		}
	}
	
	@Test
	public void shouldComputeTotalCostAfterRemovingAnAdjustment() {
		OrderAdjustment adjustment = new OrderAdjustment(OrderAdjustment.Type.FEE, 10d);
		order.addAdjustment(adjustment);
		order.persist();
		Double total = order.computeTotalCost();
		order.removeAdjustment(adjustment);
		assertEquals(total - adjustment.getValue() , order.getTotal());
	}
	
	@Test
	public void shouldComputeTotalCostAfterAddingAnAdjustment() {
		Double total = order.computeTotalCost();
		OrderAdjustment adjustment = new OrderAdjustment(OrderAdjustment.Type.FEE, 10d);
		order.addAdjustment(adjustment);
		order.persist();
		assertEquals(total + adjustment.getValue() , order.getTotal());
	}
	
	@Test
	public void shouldAddNote() {
		OrderNote note = new OrderNote("test123");
		order.addNote(note);
		order.persist();
		assertTrue(order.getNotes().contains(note));
	}
	
	@Test
	public void shouldRemoveNote() {
		OrderNote note = new OrderNote("test123");
		order.addNote(note);
		order.persist();
		order.removeNote(note);
		assertFalse(order.getNotes().contains(note));
	}
	
	@Test
	public void shouldAddRequest() {
		OrderRequest request = new OrderRequest(RequestType.cancel, "Buyer cancelled");
		order.addRequest(request);
		order.persist();
		assertTrue(order.getRequests().contains(request));
	}
	
	@Test
	public void shouldRemoveRequest() {
		OrderRequest request = new OrderRequest(RequestType.cancel, "Buyer cancelled");
		order.addRequest(request);
		order.persist();
		order.removeRequest(request);
		assertFalse(order.getRequests().contains(request));
	}
	
	@Test
	public void shouldCancelOrderIfCanBeCancelled() {
		for (Status status : Order.CANCELLABLE_STATES) {
			Order order = createOrder();
			order.setStatus(status);
			order.cancel("test123");
			assertEquals(order.getStatus(), Status.cancelled);
			assertEquals(order.getItems().iterator().next().getStatus(), OrderItem.Status.cancelled);
		}
	}
	
	@Test
	public void shouldNotCancelOrderIfCantBeCancelled() {
		for (Status status : Status.values()) {
			if (! Order.CANCELLABLE_STATES.contains(status)) {
				Order order = createOrder();
				order.setStatus(status);
				try {
					order.cancel("test123");
					fail("Expected OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldCreateOrderRequestWhenCancelled() {
		order.cancel("test reason");
		assertEquals(order.getRequests().iterator().next().getReason(), "test reason");
	}
	
	@Test
	public void shouldCancelOrderItemIfCanBeCancelled() {
		for (Status status : Order.CANCELLABLE_STATES) {
			Order order = createOrder();
			order.setStatus(status);
			OrderItem item = order.getItems().iterator().next();
			order.cancelItem(item, "test123");
			assertEquals(order.getStatus(), Status.cancelled);
			assertEquals(item.getStatus(), OrderItem.Status.cancelled);
		}
	}
	
	@Test
	public void shouldNotCancelOrderItemIfCanBeCancelled() {
		for (Status status : Status.values()) {
			if (! Order.CANCELLABLE_STATES.contains(status)) {
				Order order = createOrder();
				order.setStatus(status);
				OrderItem item = order.getItems().iterator().next();
				try {
					order.cancelItem(item, "test123");
					fail("Expected OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldApproveOrderIfInApprovableState() {
		for (Status status : Order.APPROVABLE_STATES) {
			Order order = createOrder();
			order.setStatus(status);
			order.approve();
			assertEquals(order.getStatus(), Status.approved);
			assertEquals(order.getItems().iterator().next().getStatus(), OrderItem.Status.approved);
		}
	}
	
	@Test
	public void shouldNotApproveOrderIfNotInApprovableState() {
		for (Status status : Status.values()) {
			if (! Order.APPROVABLE_STATES.contains(status)) {
				Order order = createOrder();
				order.setStatus(status);
				try {
					order.approve();
					fail("Expected OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldConfirmOrderIfInApprovedState() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next(); 
		order.approve();
		order.confirm();
		assertEquals(order.getStatus(), Status.confirmed);
		assertEquals(orderItem.getStatus(), OrderItem.Status.confirmed);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotConfirmOrderIfNotInApprovedState() {
		Order order = createOrder();
		order.confirm();
	}
	
	@Test
	public void shouldConfirmOrderItemIfInApprovedState() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		order.addItem(orderItem2);
		order.approve();
		order.confirmItem(orderItem);
		assertEquals(order.getStatus(), Status.approved);
		assertEquals(orderItem.getStatus(), OrderItem.Status.confirmed);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotConfirmOrderItemIfNotInApprovedState() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		order.confirmItem(orderItem);
	}
	
	@Test
	public void shouldConfirmOrderIfAllOrderItemsAreConfirmed() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		order.addItem(orderItem2);
		order.approve();
		order.confirmItem(orderItem);
		order.confirmItem(orderItem2);
		assertEquals(order.getStatus(), Status.confirmed);
		assertEquals(orderItem.getStatus(), OrderItem.Status.confirmed);
		assertEquals(orderItem2.getStatus(), OrderItem.Status.confirmed);
	}
	
	@Test
	public void shouldComputeOrderStatusAsCreated() {
		Order order = createOrder();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		order.addItem(orderItem2);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.created);
	}
	
	@Test
	public void shouldComputeOrderStatusAsApproved() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.approved);
		orderItem2.setStatus(OrderItem.Status.confirmed);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.approved);
	}
	
	@Test
	public void shouldComputeOrderStatusAsConfirmed() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.confirmed);
		orderItem2.setStatus(OrderItem.Status.picked);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.confirmed);
	}
	
	@Test
	public void shouldComputeOrderStatusAsDispathced() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.picked);
		orderItem2.setStatus(OrderItem.Status.delivered);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.picked);
	}
	
	@Test
	public void shouldComputeOrderStatusAsDelivered() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.delivered);
		orderItem2.setStatus(OrderItem.Status.returned);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.delivered);
	}
	
	@Test
	public void shouldComputeOrderStatusAsReturned() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.returned);
		orderItem2.setStatus(OrderItem.Status.cancelled);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.returned);
	}
	
	@Test
	public void shouldComputeOrderStatusAsCancelled() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.cancelled);
		orderItem2.setStatus(OrderItem.Status.cancelled);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.cancelled);
	}
	
	@Test
	public void shouldComputeOrderStatusAsOnHold() {
		Order order = createOrder();
		OrderItem orderItem = order.getItems().iterator().next();
		OrderItem orderItem2 = new OrderItem(3L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem2.setOrder(order);
		order.getItems().add(orderItem2);
		orderItem.setStatus(OrderItem.Status.on_hold);
		orderItem2.setStatus(OrderItem.Status.created);
		order.computeStatus();
		assertEquals(order.getStatus(), Status.on_hold);
	}
	
	private Order createOrder() {
		Order order = new Order(1L, RandomStringUtils.random(5), 2L, 3L, new Timestamp(System.currentTimeMillis()));
		OrderItem orderItem = new OrderItem(2L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis()));
		orderItem.setOrder(order);
		order.setItems(Sets.newHashSet(orderItem));
		order.persist();
		return order;
	}
}
