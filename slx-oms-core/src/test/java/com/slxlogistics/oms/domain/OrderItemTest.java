/**
 * 
 */
package com.slxlogistics.oms.domain;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.sql.Timestamp;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.slxlogistics.oms.OMSException;
import com.slxlogistics.oms.domain.OrderItem.Status;
import com.slxlogistics.oms.domain.OrderItemAdjustment.Type;

/**
 * @author ganeshs
 *
 */
public class OrderItemTest {
	
	private OrderItem item;
	
	@BeforeMethod
	public void setup() {
		item = spy(new OrderItem(2L, 3L, 2d, "Test Description", 10d, new Timestamp(System.currentTimeMillis())));
	}
	
	@Test
	public void shouldCheckIfItemCostCanBeComputed() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				assertTrue(item.canComputeTotalCost());
			} else {
				assertFalse(item.canComputeTotalCost());
			}
		}
	}

	@Test
	public void shouldCheckIfItemCanBeApproved() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.APPROVABLE_STATES.contains(status)) {
				assertTrue(item.canApprove());
			} else {
				assertFalse(item.canApprove());
			}
		}
	}
	
	@Test
	public void shouldCheckIfItemCanBeConfirmed() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (status == Status.approved) {
				assertTrue(item.canConfirm());
			} else {
				assertFalse(item.canConfirm());
			}
		}
	}
	
	@Test
	public void shouldCheckExceptionThrownIfItemCantBeCancelled() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.CANCELLABLE_STATES.contains(status)) {
				assertTrue(item.canCancel(true));
			} else {
				try {
					item.canCancel(true);
					fail("Expected OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldApproveItemIfCanBeApproved() {
		doReturn(true).when(item).canApprove();
		item.approve();
		assertEquals(item.getStatus(), Status.approved);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotApproveItemIfCantBeApproved() {
		doReturn(false).when(item).canApprove();
		item.approve();
	}
	
	@Test
	public void shouldConfirmItemIfCanBeConfirmed() {
		doReturn(true).when(item).canConfirm();
		item.confirm();
		assertEquals(item.getStatus(), Status.confirmed);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotConfirmItemIfCantBeConfirmed() {
		doReturn(false).when(item).canConfirm();
		item.confirm();
	}
	
	@Test
	public void shouldCancelItemIfCanBeCancelled() {
		doReturn(true).when(item).canCancel(true);
		item.cancel("test reason");
		assertEquals(item.getStatus(), Status.cancelled);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotCancelItemIfCantBeCancelled() {
		doThrow(OMSException.class).when(item).canCancel(true);
		item.cancel("test reason");
	}
	
	@Test
	public void shouldCreateItemRequestOnCancellation() {
		doReturn(true).when(item).canCancel(true);
		item.cancel("test reason");
		OrderItemRequest request = item.getRequests().iterator().next();
		assertEquals(request.getReason(), "test reason");
	}
	
	@Test
	public void shouldAddNote() {
		OrderItemNote note = new OrderItemNote("test123");
		item.addNote(note);
		assertTrue(item.getNotes().contains(note));
	}
	
	@Test
	public void shouldRemoveNote() {
		OrderItemNote note = new OrderItemNote("test123");
		item.addNote(note);
		item.removeNote(note);
		assertFalse(item.getNotes().contains(note));
	}
	
	@Test
	public void shouldAddRequest() {
		OrderItemRequest request = new OrderItemRequest(RequestType.cancel, "Buyer cancelled");
		item.addRequest(request);
		assertTrue(item.getRequests().contains(request));
	}
	
	@Test
	public void shouldRemoveRequest() {
		OrderItemRequest request = new OrderItemRequest(RequestType.cancel, "Buyer cancelled");
		item.addRequest(request);
		item.removeRequest(request);
		assertFalse(item.getRequests().contains(request));
	}
	
	@Test
	public void shouldAddAdjustmentsIfCostCanBeComputed() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		assertTrue(item.getAdjustments().contains(adjustment));
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotAddAdjustmentsIfCostCantBeComputed() {
		doReturn(false).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
	}
	
	@Test
	public void shouldRemoveAdjustmentsIfCostCanBeComputed() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		item.removeAdjustment(adjustment);
		assertFalse(item.getAdjustments().contains(adjustment));
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotRemoveAdjustmentsIfCostCantBeComputed() {
		doReturn(false).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.removeAdjustment(adjustment);
	}
	
	@Test
	public void shouldComputeTotalCostAfterAddingAdjustments() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		assertEquals(item.getTotal(), item.getPrice() + 10d);
	}
	
	@Test
	public void shouldComputeTotalCost() {
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		assertEquals(item.computeTotalCost(), item.getPrice() + 10d);
	}
	
	@Test
	public void shouldComputeTotalCostAfterRemovingAdjustments() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		item.removeAdjustment(adjustment);
		assertEquals(item.getTotal(), item.getPrice());
	}
}
