package com.slxlogistics.oms.partner;

import com.slx.communication.client.CommunicationClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by nitinka on 10/11/14.
 */
public abstract class AbstractMailClient extends PartnerClient {
    @Autowired
    protected CommunicationClient communicationClient;

    public AbstractMailClient(Map<String, Object> config) {
        super(config);
    }
}
