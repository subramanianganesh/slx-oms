package com.slxlogistics.oms.partner;

import com.slxlogistics.oms.domain.OrderItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by nitinka on 10/11/14.
 */
public class FirstFlightMailClient extends AbstractHttpClient {
    private static Logger logger = LoggerFactory.getLogger(FirstFlightMailClient.class);

    public FirstFlightMailClient(Map<String, Object> config) {
        super(config);
    }

    @Override
    public TrackingDetail publish(OrderItem orderItem) {
        logger.info("Doing Single Mail Publish");
        return null;
    }

    @Override
    public Map<Integer, TrackingDetail> publish(List<OrderItem> orderItems) {
        logger.info("Doing Multi Mail Publish");
        return null;
    }
}
