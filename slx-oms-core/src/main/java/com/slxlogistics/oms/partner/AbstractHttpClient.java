package com.slxlogistics.oms.partner;

import com.ning.http.client.AsyncHttpClient;

import java.util.Map;

/**
 * Created by nitinka on 10/11/14.
 */
public abstract class AbstractHttpClient extends PartnerClient {
    protected AsyncHttpClient httpClient = new AsyncHttpClient();
    private final String baseUrl;

    public AbstractHttpClient(Map<String, Object> config) {
        super(config);
        this.baseUrl = config.get("baseUrl").toString();
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
