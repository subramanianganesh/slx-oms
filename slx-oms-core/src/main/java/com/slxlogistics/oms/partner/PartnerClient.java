package com.slxlogistics.oms.partner;

import com.slxlogistics.oms.domain.OrderItem;

import java.util.List;
import java.util.Map;

/**
 * Created by nitinka on 10/11/14.
 */
abstract public class PartnerClient {
    private final Map<String, Object> config;

    public PartnerClient(Map<String, Object> config) {
        this.config = config;
    }
    public abstract TrackingDetail publish(OrderItem orderItem);
    public abstract Map<Integer, TrackingDetail> publish(List<OrderItem> orderItems);

    public Map<String, Object> getConfig() {
        return config;
    }
}
