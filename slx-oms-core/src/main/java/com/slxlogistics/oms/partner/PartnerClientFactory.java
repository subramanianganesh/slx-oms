package com.slxlogistics.oms.partner;

import com.slxlogistics.oms.config.PartnerPublisherConfig;
import com.slxlogistics.oms.util.ClassHelper;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nitinka on 10/11/14.
 */
public class PartnerClientFactory {
    private static Map<String, PartnerClient> partnerClients = new HashMap<String, PartnerClient>();

    public static void initializePartnerClient(String clientName, PartnerPublisherConfig partnerClientConfig) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        PartnerClient partnerClient = partnerClients.get(clientName);
        if(partnerClient == null) {
            partnerClient = (PartnerClient) ClassHelper.createInstance(Class.forName(partnerClientConfig.getPublisherClass())
                    , new Class[]{Map.class}, new Object[]{partnerClientConfig.getConfig()});
            partnerClients.put(clientName, partnerClient);
        }
    }

    public static PartnerClient getClient(String partnerClientName) {
        return partnerClients.get(partnerClientName);
    }
}
