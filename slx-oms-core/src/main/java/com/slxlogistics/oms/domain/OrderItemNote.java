/**
 * 
 */
package com.slxlogistics.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @createdBy ganeshs
 *
 */
@Entity
@Table(name="order_item_notes")
@Access(AccessType.FIELD)
public class OrderItemNote extends BaseRecordableDomain {

	@NotNull
	private String note;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderItemId")
	@JsonBackReference("notes")
	private OrderItem item;
	
	private String createdBy;
	
	/**
	 * Default constructor
	 */
	public OrderItemNote() {
	}

	/**
	 * @param note
	 */
	public OrderItemNote(String note) {
		this.note = note;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the item
	 */
	public OrderItem getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(OrderItem item) {
		this.item = item;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
