/**
 * 
 */
package com.slxlogistics.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="order_item_adjustments")
@Access(AccessType.FIELD)
public class OrderItemAdjustment extends BaseRecordableDomain {

	/**
	 * @author ganeshs
	 *
	 */
	public enum Type {
		shipping(true), tax(true);
		
		private boolean unitLevel;
		
		/**
		 * @param unitLevel
		 */
		private Type(boolean unitLevel) {
			this.unitLevel = unitLevel;
		}

		/**
		 * @return the unitLevel
		 */
		public boolean isUnitLevel() {
			return unitLevel;
		}

		/**
		 * @param unitLevel the unitLevel to set
		 */
		public void setUnitLevel(boolean unitLevel) {
			this.unitLevel = unitLevel;
		}
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="order_item_id")
	@JsonBackReference("adjustments")
	private OrderItem item;
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private Type type;
	
	@NotNull
	private Double value;
	
	/**
	 * Default constructor
	 */
	public OrderItemAdjustment() {
	}

	/**
	 * @param type
	 * @param value
	 */
	public OrderItemAdjustment(Type type, Double value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * @return the item
	 */
	public OrderItem getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(OrderItem item) {
		this.item = item;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}
//	
//	public Double valueFor(int quantity) {
//		return type.isUnitLevel() ? quantity * value : value;
//	}
}
