/**
 * 
 */
package com.slxlogistics.oms.domain;

import java.sql.Timestamp;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="payments")
@Access(AccessType.FIELD)
public class Payment extends BaseRecordableDomain {
    
    /**
     * @author ganeshs
     *
     */
    public enum Type {
        payment, refund
    }
    
    /**
     * @author ganeshs
     *
     */
    public enum Status {
        initiated, cancelled, received, failed, successful
    }
    
    /**
     * @author ganeshs
     *
     */
    public enum Method {
        credit_card, debit_card, netbanking, wallet
    }

    @NotNull
    private Long userId;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type = Type.payment;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status = Status.initiated;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    private Method method;
    
    private String gateway;
    
    private String transactionId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="order_id")
    @JsonBackReference("payments")
    private Order order;
    
    @NotNull
    private Double amount;
    
    private Double actualAmount;
    
    private Timestamp recordedDate;

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the method
     */
    public Method getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(Method method) {
        this.method = method;
    }

    /**
     * @return the gateway
     */
    public String getGateway() {
        return gateway;
    }

    /**
     * @param gateway the gateway to set
     */
    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the recordedDate
     */
    public Timestamp getRecordedDate() {
        return recordedDate;
    }

    /**
     * @param recordedDate the recordedDate to set
     */
    public void setRecordedDate(Timestamp recordedDate) {
        this.recordedDate = recordedDate;
    }

    /**
     * @return the actualAmount
     */
    public Double getActualAmount() {
        return actualAmount;
    }

    /**
     * @param actualAmount the actualAmount to set
     */
    public void setActualAmount(Double actualAmount) {
        this.actualAmount = actualAmount;
    }
}
