/**
 * 
 */
package com.slxlogistics.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="order_requests")
@Access(AccessType.FIELD)
public class OrderRequest extends BaseRecordableDomain {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderId")
	@JsonBackReference("requests")
	private Order order;
	
	private String reason;
	
	@Enumerated(EnumType.STRING)
	private RequestType type;
	
	private String createdBy;
	
	/**
	 * Default constructor
	 */
	public OrderRequest() {
	}
	
	/**
	 * @param type
	 * @param reason
	 */
	public OrderRequest(RequestType type, String reason) {
		this.reason = reason;
		this.type = type;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the type
	 */
	public RequestType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(RequestType type) {
		this.type = type;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
