/**
 * 
 */
package com.slxlogistics.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="order_adjustments")
@Access(AccessType.FIELD)
public class OrderAdjustment extends BaseRecordableDomain {

	/**
	 * @author ganeshs
	 *
	 */
	public enum Type {
		CASHBACK, GIFTWRAP, FEE
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderId")
	@JsonBackReference("adjustments")
	private Order order;
	
	@Enumerated
	@NotNull
	private Type type;
	
	@NotNull
	private Double value;
	
	/**
	 * Default constructor
	 */
	public OrderAdjustment() {
	}
	
	/**
	 * @param type
	 * @param value
	 */
	public OrderAdjustment(Type type, Double value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}
}
