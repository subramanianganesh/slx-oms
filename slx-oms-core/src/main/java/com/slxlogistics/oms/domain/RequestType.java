/**
 * 
 */
package com.slxlogistics.oms.domain;

/**
 * @author ganeshs
 *
 */
public enum RequestType {

	cancel, hold
}
