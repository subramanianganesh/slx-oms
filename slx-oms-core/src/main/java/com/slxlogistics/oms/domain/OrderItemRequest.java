/**
 * 
 */
package com.slxlogistics.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="order_item_requests")
@Access(AccessType.FIELD)
public class OrderItemRequest extends BaseRecordableDomain {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderItemId")
	@JsonBackReference("requests")
	private OrderItem item;
	
	private String reason;
	
	private String createdBy;
	
	@Enumerated(EnumType.STRING)
	private RequestType type;
	
	/**
	 * Default constructor
	 */
	public OrderItemRequest() {
	}

	/**
	 * @param type
	 * @param quantity
	 * @param successfulQuantity
	 * @param reason
	 */
	public OrderItemRequest(RequestType type, String reason) {
		this.reason = reason;
		this.type = type;
	}

	/**
	 * @return the item
	 */
	public OrderItem getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(OrderItem item) {
		this.item = item;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the type
	 */
	public RequestType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(RequestType type) {
		this.type = type;
	}
}
