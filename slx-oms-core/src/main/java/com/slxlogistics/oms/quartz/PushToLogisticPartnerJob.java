package com.slxlogistics.oms.quartz;

import com.slxlogistics.oms.domain.OrderItem;
import com.slxlogistics.oms.partner.PartnerClient;
import com.slxlogistics.oms.partner.PartnerClientFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitinka on 10/11/14.
 */
public class PushToLogisticPartnerJob implements Job{
    private static Logger logger = LoggerFactory.getLogger(PushToLogisticPartnerJob.class);
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Running");
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        Integer fetchSize = (Integer)jobDataMap.get("fetchSize");
        Integer publishSize = (Integer)jobDataMap.get("publishSize");

        List<OrderItem> orderItems = fetchOrderItems(fetchSize);
        PartnerClient partnerClient = PartnerClientFactory.getClient(jobDataMap.getString("partnerClientName"));
        logger.info("Partner Client: "+partnerClient.getClass().getCanonicalName());

        while(orderItems.size() > 0){
            if(publishSize == 1) {
                partnerClient.publish(new OrderItem());
            }
            else {
                partnerClient.publish(new ArrayList<OrderItem>());
            }
            orderItems = fetchOrderItems(fetchSize);
        }
    }

    private List<OrderItem> fetchOrderItems(Integer fetchSize) {
        return new ArrayList<OrderItem>();
    }
}
