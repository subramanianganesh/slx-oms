/**
 * 
 */
package com.slxlogistics.oms;

import javax.ws.rs.WebApplicationException;


/**
 * @author ganeshs
 *
 */
public class OMSException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	/**
	 * @author ganeshs
	 *
	 */
	public enum ErrorCode {
		
		bad_state(422), insufficient_quantity(422);
		
		private int status;
		
		/**
		 * @param status
		 */
		private ErrorCode(int status) {
			this.status = status;
		}
	}

	/**
	 * @param code
	 * @param message
	 */
	public OMSException(ErrorCode code, String message) {
		super(message, code.status);
	}
	
	/**
	 * @param code
	 * @param message
	 * @param cause
	 */
	public OMSException(ErrorCode code, String message, Throwable cause) {
		super(message, cause, code.status);
	}
}
