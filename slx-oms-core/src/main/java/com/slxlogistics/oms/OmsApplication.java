/**
 * 
 */
package com.slxlogistics.oms;

import com.slxlogistics.core.SlxApplication;
import com.slxlogistics.oms.config.PartnerPublisherConfig;
import com.slxlogistics.oms.config.PartnerPublishConfig;
import com.slxlogistics.oms.partner.PartnerClientFactory;
import com.slxlogistics.oms.quartz.PushToLogisticPartnerJob;
import com.slxlogistics.spring.util.SpringUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author ganeshs
 *
 */
public class OmsApplication extends SlxApplication<OmsConfiguration> {
    private static Logger logger = LoggerFactory.getLogger(OmsApplication.class);

    @Override
    public void init() {
        super.init();
        SpringUtil.load(SpringConfig.class);
        try {
            initializePartnerPublishing(getConfiguration().getPartnerPublishConfig());
        } catch (Exception e) {
            logger.error("Failed to initialize Partner Publishers", e);
            throw new RuntimeException("Failed to initialize Partner Publishers", e);
        }
    }

    private void initializePartnerPublishing(PartnerPublishConfig partnerPublishConfig)
            throws SchedulerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        if(partnerPublishConfig.isEnabled()) {
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();
            Map<String,PartnerPublisherConfig> publishers = partnerPublishConfig.getPublishers();
            for(String publisher : publishers.keySet()) {
                PartnerPublisherConfig publisherConfig = publishers.get(publisher);
                if(publisherConfig.isEnabled()) {
                    PartnerClientFactory.initializePartnerClient(publisher, publisherConfig);

                    JobDetail job = newJob(PushToLogisticPartnerJob.class)
                            .withIdentity(publisher, "PartnerPublisher")
                            .usingJobData("partnerClientName", publisher)
                            .usingJobData("fetchSize", publisherConfig.getFetchSize())
                            .usingJobData("publishSize", publisherConfig.getPublishSize())
                            .build();

                    CronTrigger trigger = newTrigger()
                            .withIdentity(publisher, "PartnerPublisher")
                            .withSchedule(cronSchedule(publisherConfig.getScheduleCronInfo()))
                            .build();

                    scheduler.scheduleJob(job, trigger);
                }
            }

            scheduler.start();
        }
    }



    @Override
    public void stop() {
        super.stop();
    }

    public static void main(String[] args) {
        org.minnal.Bootstrap.main(args);
    }
}
