package com.slxlogistics.oms.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by nitinka on 11/11/14.
 */
public class ClassHelper {
    public static <T> T createInstance(Class<T> type) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        return createInstance(type, new Class[]{}, new Object[]{});
    }

    public static <T> T createInstance(Class<T> type, Class[] paramTypes, Object[] params) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Constructor<T> constructor = type.getConstructor(paramTypes);
        return constructor.newInstance(params);
    }

    public static Object createInstance(String clazz, Class[] paramTypes, Object[] params) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class instanceClass = Class.forName(clazz);
        Constructor cons    =   instanceClass.getConstructor(paramTypes);
        return cons.newInstance(params);
    }
}
