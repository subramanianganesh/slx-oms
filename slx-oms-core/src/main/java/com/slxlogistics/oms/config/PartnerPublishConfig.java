package com.slxlogistics.oms.config;

import java.util.Map;

/**
 * Created by nitinka on 11/11/14.
 */
public class PartnerPublishConfig {
    private boolean enabled;
    private Map<String, PartnerPublisherConfig> publishers;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, PartnerPublisherConfig> getPublishers() {
        return publishers;
    }

    public void setPublishers(Map<String, PartnerPublisherConfig> publishers) {
        this.publishers = publishers;
    }
}
