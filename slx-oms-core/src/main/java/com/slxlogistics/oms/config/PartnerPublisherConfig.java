package com.slxlogistics.oms.config;

import org.quartz.CronExpression;

import java.util.Map;

/**
 * Created by nitinka on 11/11/14.
 */
public class PartnerPublisherConfig {
    private boolean enabled;
    private int fetchSize;
    private int publishSize;
    private String publisherClass;
    private Map<String, Object> config;
    private CronExpression scheduleCronInfo;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getFetchSize() {
        return fetchSize;
    }

    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    public int getPublishSize() {
        return publishSize;
    }

    public void setPublishSize(int publishSize) {
        this.publishSize = publishSize;
    }

    public String getPublisherClass() {
        return publisherClass;
    }

    public void setPublisherClass(String publisherClass) {
        this.publisherClass = publisherClass;
    }

    public Map<String, Object> getConfig() {
        return config;
    }

    public void setConfig(Map<String, Object> config) {
        this.config = config;
    }

    public CronExpression getScheduleCronInfo() {
        return scheduleCronInfo;
    }

    public void setScheduleCronInfo(CronExpression scheduleCronInfo) {
        this.scheduleCronInfo = scheduleCronInfo;
    }
}
