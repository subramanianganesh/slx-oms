/**
 * 
 */
package com.slxlogistics.oms;

import com.slxlogistics.core.jms.JMSConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

/**
 * @author nitinka
 * 
 */
@Configuration
@ComponentScan(basePackages = { "com.slxlogistics" })
public class SpringConfig extends JMSConfig {

    public SpringConfig() {
        super(new ClassPathResource("/META-INF/oms.yml"));
    }

//    @Bean
//    public PartnerClientFactory getPartnerPublishFactory() {
//        return null;
//    }
}
