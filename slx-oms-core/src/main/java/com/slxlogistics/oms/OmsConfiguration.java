/**
 * 
 */
package com.slxlogistics.oms;

import com.slxlogistics.oms.config.PartnerPublishConfig;
import org.minnal.core.config.ApplicationConfiguration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author ganeshs
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class OmsConfiguration extends ApplicationConfiguration {
    private PartnerPublishConfig partnerPublishConfig;

    /**
     * Default constructor
     */
    public OmsConfiguration() {
    }

    /**
     * @param name
     */
    public OmsConfiguration(String name) {
        super(name);
    }

    public PartnerPublishConfig getPartnerPublishConfig() {
        return partnerPublishConfig;
    }

    public void setPartnerPublishConfig(PartnerPublishConfig partnerPublishConfig) {
        this.partnerPublishConfig = partnerPublishConfig;
    }
}
