alter table order_items add column deliver_slot_begin_time timestamp null default 0;
alter table order_items add column deliver_slot_end_time timestamp null default 0;
alter table order_items add column pickup_slot_begin_time timestamp null default 0;
alter table order_items add column pickup_slot_end_time timestamp null default 0;
alter table order_items add column tracking_id varchar(50);