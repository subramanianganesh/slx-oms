create table payments (
  id bigint(20) not null primary key auto_increment,
  user_id bigint(20) not null,
  order_id bigint(20) not null,
  `type` varchar(20) not null,
  status varchar(20) not null,
  method varchar(20) not null,
  gateway varchar(20),
  transaction_id varchar(50),
  amount double not null default 0,
  recorded_date timestamp not null default 0,
  deleted tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);
